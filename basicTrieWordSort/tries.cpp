
#include <iostream>
#include <vector>
#include <string>
#include <cctype>
#include <ctime>

const int ALPHABET = 26;

using namespace std;

struct node
{
  struct node * parent;
  struct node * children[ALPHABET];
  bool hasWord = false;
};

void insertWord (struct node * trieTree, string word) {

  struct node * traverse = trieTree;

  int wordLength = word.size();
  int i = 0, curIndex = 0;

  while (i < wordLength) {
    curIndex = (word.at(i) - 'a');
    if (traverse->children[curIndex] == NULL) {
      traverse->children[curIndex] = new node;
      traverse->children[curIndex]->parent = traverse;
    }
    traverse = traverse->children[curIndex];
    ++i;
  }

  traverse->hasWord = true;

}

struct node * searchAndReturnWord (struct node * trieNode, string word, string& bestMatch, string& prefix) {

  int i = 0, curIndex = 0;
  int wordLength = word.size();

  while (i < wordLength) {
    curIndex = (word.at(i) - 'a');
    if (trieNode->children[curIndex] != NULL) {
      prefix.push_back(word.at(i));
      trieNode = trieNode->children[curIndex];
      if (trieNode->hasWord == true)
	bestMatch = prefix;
      else {
	std::cout << "current node does not signify a word" << std::endl;
      }
      ++i;
    }
    else {
      break;
    }
  }

  if (i == wordLength && trieNode->hasWord == true)
    return trieNode;

  std::cout << "did not find " << word << std::endl;
  return NULL;

}

struct node * searchWord (struct node * trieNode, string word) {

  int i = 0, curIndex = 0;
  int wordLength = word.size();

  while (i < wordLength) {
    curIndex = (word.at(i) - 'a');
    if (trieNode->children[curIndex] != NULL) {
      trieNode = trieNode->children[curIndex];
      ++i;
    }
    else {
      break;
    }
  }

  if (i == wordLength && trieNode->hasWord == true)
    return trieNode;

  return NULL;

}

void removeWord (struct node * trieTree, string word) {

  struct node * searchNode = searchWord(trieTree, word);

  if (searchNode == NULL)
    return;

  searchNode->hasWord = false;

  bool hasChild = false;

  for (int i = 0; i < ALPHABET; ++i) {
    if (searchNode->children[i] != NULL) {
      hasChild = true;
      break;
    }
  }

  if (hasChild)
    return;

  struct node * parentNode;
  int childCount = 0;

  while (searchNode->hasWord == false && searchNode->parent != NULL && childCount == 0) {
  
    parentNode = searchNode->parent;

    for (int i = 0; i < ALPHABET; ++i) {
      if (parentNode->children[i] != NULL) {
	if (searchNode == parentNode->children[i]) {
	  parentNode->children[i] = NULL;
	  delete searchNode;
	  searchNode = parentNode;
	}
	else {
	  ++childCount;
	}
      }
    }
  }
}

void printTree(struct node * trieNode, string& prefix) {
  
  if (trieNode->hasWord)
    cout << prefix << endl;

  for (char index = 0; index < ALPHABET; ++index) {
    char next = 'a' + index;
    struct node * child = trieNode->children[index];
    if (child != NULL) {
      prefix.push_back(next);
      printTree(child, prefix);
      prefix.pop_back();
    }
  }
}

void cleanInput(string& word) {

  int wordLength = word.size();

  for (int i = 0; i < wordLength; i++) {

    if (i < 0)
      i = 0;

    if (isupper(word.at(i)))
      word.at(i) = tolower(word.at(i));

    if (!isalpha(word.at(i))) {
      if (i == 0 && wordLength != 0) {
	word = word.substr(1);
	--wordLength;
	--i;
      }
      else if (i <= wordLength - 1) {
	word = word.substr(0, i) + word.substr(i+1);
	--wordLength;
	--i;
      }
    }
  }
}

int main () {

  cout << "Please enter a list of words to be sorted in alphabetical order." << endl;
  cout << "Words will be converted to all lower case. Non letter characters will be removed." << endl;
  cout << "Enter [Done] as a word when you are done entering words." << endl;

  bool entering = true;
  string entry;

  struct node * myTree = new node;

  std::vector<std::string> wordsEntered;
  
  while (entering) {
    getline(cin, entry);

    if (entry == "[Done]")
      break;

    cleanInput(entry);

    insertWord(myTree, entry);

    wordsEntered.push_back(entry);
  }

  std::cout << "--------------------------------------------------------------------" << std::endl;
  std::cout << "Enter a word to search the trie for. We will return the best match." << std::endl;
  std::cout << "Word: " << std::endl;
  std::string input;
  std::string searchPrefix = "";
  std::string bestMatch = "";
  getline(std::cin, input);
  searchAndReturnWord(myTree, input, bestMatch, searchPrefix);
  std::cout << "--------------------------------------------------------------------" << std::endl;
  std::cout << "We searched for " << input << " in the trie." << std::endl;
  if (bestMatch == "")
    std::cout << "There were no prefixes of " << input << " in the trie." << std::endl;
  else
    std::cout << "We found a best matching prefix of " << bestMatch << " in the trie." << std::endl;

  std::cout << "----------------------------------------------------------------------" << std::endl;
  std::cout << "Printing entire tree in lexographic order... " << std::endl;
  std::cout << "---" << std::endl;
  string prefix;
  printTree(myTree, prefix);
  std::cout << "---" << std::endl;

  std::vector<std::string>::iterator wordsItr = wordsEntered.begin();

  for (wordsItr; wordsItr != wordsEntered.end(); wordsItr++) {
    removeWord(myTree, *wordsItr);
  }

  delete myTree;

}
