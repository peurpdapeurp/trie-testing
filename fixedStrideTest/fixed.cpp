
/*
List of things to do:
*** Just a weird thing, the textbook mentions that a 1bit trie is kept for each node in the trie for the purposes of insertion
and deletion. I don't really see how that works, will have to research that...
1) Get the idea of prefix expansion sorted out:
      - should search / removal of non stride multiple length addresses be allowed?
            - if removal is allowed, how will you handle the ambiguity of prefix expansions where one expansion is overwritten
	    - if search is allowed, how will you handle ambiguity of which expansion should be considered "found"?
      - FOR NOW, JUST NOT GOING TO ALLLOW SEARCH OR REMOVAL UNLESS IT IS A STRIDE MULTIPLE
2) Start to implement variable stride
      - figure out how the level optimization algorithm works, still don't understand that
3) Work on implementing the lulea compressed trie
      - figure out doing bitmap compression
      - 

Other things to think about:
1) What is the time complexity of insertion, search, and removal for the different types of tries?
      - (unibit, fixed stride, variable stride, lulea compressed, bitmap compressed, etc)
2) Which of these specific types of tries are typically used in implementations of network address search?
3) Are they used in ndn? To what capacity?
*/

#include <iostream>
#include <string>

const int STRIDE = 3;
const int NODESIZE = 1 << STRIDE;

int btod (std::string binary) {
  int decimal = 0;
  int binarySize = binary.size();
  for (int i = 0; i < binarySize; i++) {
    decimal += (binary.at(i) - '0') * (1 << (binarySize - 1 - i));
  }
  return decimal;
}

struct block {
  struct node * child;
  struct node * parent;
  std::string routeInfo;
};

struct node {
  struct block * parent;
  struct block * children[NODESIZE];
};

struct searchResult {
  struct block * blockPointer;
  // this is the pointer to the lmp; will be the same as blockPointer if there is an exact match
  struct block * lmpBlockPointer;
  std::string lmp; // longest matching prefix
};

void insertAddress (struct node * trieTree, std::string address, std::string routeInfo) {

  struct node * traverse = trieTree;

  int addressSize = address.size();
  int itr = 0;

  if (addressSize % STRIDE != 0) {
    std::cout << "Address length was not an integer multiple of the stride, " << STRIDE << "." << std::endl;
    address.push_back('0');
    insertAddress(trieTree, address, routeInfo);
    address.pop_back();
    address.push_back('1');
    insertAddress(trieTree, address, routeInfo);
    address.pop_back();
    return;
  }

  while (itr < addressSize/STRIDE) {
    
    int curIndex = btod(address.substr(itr*STRIDE, STRIDE));

    //std::cout << "Current iteration: " << itr << std::endl;
    //std::cout << "Current index: " << curIndex << std::endl;
    //std::cout << "Current address chunk: " << address.substr(itr*STRIDE, STRIDE) << std::endl;

    if (traverse->children[curIndex] == NULL) {
      traverse->children[curIndex] = new block;
      traverse->children[curIndex]->parent = traverse;
    }

    if (itr == (addressSize/STRIDE -1)) {
      //std::cout << "We reached the last chunk of the address." << std::endl;
      if (traverse->children[curIndex]->routeInfo == "")
	// will only add routing info if the prefix didn't already have it
	traverse->children[curIndex]->routeInfo = routeInfo;
      //std::cout << "Inserted routing info " << traverse->children[curIndex]->routeInfo << "." << std::endl;
    }
    else {
      if (traverse->children[curIndex]->child == NULL) {
	traverse->children[curIndex]->child = new node;
	traverse->children[curIndex]->child->parent = traverse->children[curIndex];
      }
    }
    
    traverse = traverse->children[curIndex]->child;
    itr++;

  }

  //std::cout << "---" << std::endl;

}

struct searchResult searchAddress (struct node * trieNode, std::string address) {

  searchResult result;

  struct node * traverse = trieNode;

  int addressLength = address.size();
  int itr = 0;

  if (addressLength % STRIDE != 0) {
    std::cout << "Can only search for addresses whose length are an integer multiple of " << STRIDE << "." << std::endl;
    return result;
  }

  int curIndex = 0;

  //std::cout << "Searching for " << address << " in trie..." << std::endl;
  //std::cout << "Number of size " << STRIDE << " chunks in address: " << address.size()/STRIDE << std::endl;
  //std::cout << "---------------------------------------" << std::endl;

  while (itr < addressLength/STRIDE) {

    //std::cout << "Current itr value: " << itr << std::endl;

    curIndex = btod(address.substr(itr*STRIDE, STRIDE));

    //std::cout << "Current index value: " << curIndex << std::endl;
    //std::cout << "Current address chunk: " << address.substr(itr*STRIDE, STRIDE) << std::endl;

    if (traverse->children[curIndex] != NULL) {

      if (traverse->children[curIndex]->routeInfo != "") {
	result.lmpBlockPointer = traverse->children[curIndex];
	result.lmp = traverse->children[curIndex]->routeInfo;
      }

      if (traverse->children[curIndex]->child != NULL) {
	//std::cout << "Found a block corresponding to current chunk of address, going deeper into trie..." << std::endl;
	if (itr != addressLength/STRIDE - 1)
	  traverse = traverse->children[curIndex]->child;
      } else {
	//std::cout << "Found a corresponding block but that block had no child." << std::endl;
      }
    }
    else {
      //std::cout << "Didn't find a block corresponding to current chunk of address, exiting loop..." << std::endl;
      break;
    }

    itr++;
    
  }

  //std::cout << "Itr value after exiting loop: " << itr << std::endl;

  if (itr == addressLength/STRIDE) {
    if (traverse->children[curIndex]->routeInfo != "") {
      result.lmp = traverse->children[curIndex]->routeInfo;
      result.lmpBlockPointer = traverse->children[curIndex];
      result.blockPointer = traverse->children[curIndex];
      return result;
    }
    else {
      //std::cout << "The block corresponding to the end of the address had no routing info." << std::endl;
      result.blockPointer = traverse->children[curIndex];
      return result;
    }
  }

  //std::cout << "Search terminated before entire address was checked." << std::endl;
  return result;

}

void removeAddress (struct node * trieTree, std::string address) {

  int addressSize = address.size();
  
  if (addressSize % STRIDE != 0) {
    std::cout << "Can only remove addresses whose length are an integer multiple of " << STRIDE << "." << std::endl;
    return;
  }

  searchResult result = searchAddress(trieTree, address);
  
  struct block * searchResult = result.blockPointer;

  if (searchResult == NULL) {
    std::cout << address << " was not in the trie." << std::endl;
    return;
  }

  struct node * blockParent = searchResult->parent;

  // if the block of the address has a child, then it was the prefix of another address, so we can just set the
  // routing info corresponding to it to the empty string and exit
  if (searchResult->child != NULL) {
    searchResult->routeInfo = "";
    return;
  }

  int itr = address.size();
  int curIndex = btod(address.substr(itr-STRIDE, STRIDE));
  //std::cout << "In removal function:" << std::endl;
  //std::cout << curIndex << std::endl;
  // since the block had no child, it can be removed from the parent address
  delete searchResult;
  blockParent->children[curIndex] = NULL;

  // now check to see if the block's parent had any other children, if it did then we can just exit, if not then
  // we have to delete it and continue checking the rest of the trie
  for (int i = 0; i < NODESIZE; i++) {
    if (blockParent->children[i] != NULL) {
      std::cout << "The parent node of the original block had other children, done with removal..." << std::endl;
      return;
    }
  }

  /* sets up variables for loop and also deletes the empty node*/
  struct node * currentNode = blockParent->parent->parent;
  delete blockParent;
  struct node * parentNode;
  // decrements from itr so that the currentIndex reflects the second to last chunk of the address;
  // itr will continue to be decremented by STRIDE so that the address is traversed backwards
  itr -= STRIDE;

  while (currentNode->parent != NULL && itr - STRIDE >= 0) {

    parentNode = currentNode->parent->parent;
    curIndex = btod(address.substr(itr - STRIDE, STRIDE));

    if (currentNode->children[curIndex]->routeInfo == "") {
      delete currentNode->children[curIndex];
      currentNode->children[curIndex] = NULL;
    } else {
      return;
    }

    for (int i = 0; i < NODESIZE; i++) {
      if (currentNode->children[i] != NULL) {
	return;
      }
    }

    currentNode = currentNode->parent->parent;
    itr -= STRIDE;

  }

}

void printTree (struct node * trieNode, std::string indentation) {

  for (int i = 0; i < NODESIZE; i++) {
    if (trieNode->children[i] != NULL && trieNode->children[i]->routeInfo != "")
      std::cout << indentation << trieNode->children[i]->routeInfo << std::endl;
  }

  for (int i = 0; i < NODESIZE; i++) {
    if (trieNode->children[i] != NULL && trieNode->children[i]->child != NULL) {
      printTree(trieNode->children[i]->child, indentation + " ");
    }
  }

}

int main () {

  std::string addresses[10];

  addresses[0] = "1001001001001";
  addresses[1] = "100101111110011";
  addresses[2] = "100100010011110011";
  addresses[3] = "100100110010011110001";
  addresses[4] = "100100100100111110010";
  addresses[5] = "100100100100111100111";
  addresses[6] = "100100100010101001111001";
  addresses[7] = "100100100100110001110011";
  addresses[8] = "100101010101010100100111101011";
  addresses[9] = "100100100100100100100100100100100";

  std::string routingInfo[10];

  routingInfo[0] = "P0";
  routingInfo[1] = "P1";
  routingInfo[2] = "P2";
  routingInfo[3] = "P3";
  routingInfo[4] = "P4";
  routingInfo[5] = "P5";
  routingInfo[6] = "P6";
  routingInfo[7] = "P7";
  routingInfo[8] = "P8";
  routingInfo[9] = "P9";

  struct node * myTree = new node;

  for (int i = 0; i < 10; i++) {
    insertAddress(myTree, addresses[i], routingInfo[i]);
  }

  std::cout << std::endl << "Printing tree..." << std::endl;
  std::cout << "--------------------" << std::endl;

  std::string indentation = "";

  printTree(myTree, indentation);

  std::cout << "--------------------" << std::endl << std::endl;

  std::string testAddress = "100101111110011111";
  searchResult test = searchAddress(myTree, testAddress);
  std::cout << "Longest matching prefix for " << testAddress << ": " << test.lmp << std::endl;// << std::endl;
  std::cout << "Longest matching prefix (using lmpBlockPointer): " << test.lmpBlockPointer->routeInfo << std::endl << std::endl;

  std::cout << "--------------------" << std::endl << std::endl;

  std::cout << "Removing addresses..." /*<< address1 << " from trie..." */<< std::endl << std::endl;
  std::cout << "--------------------" << std::endl;

  for (int i = 0; i < 10; i++) {
    removeAddress(myTree, addresses[i]);
  }

  std::cout << std::endl << "Printing tree..." << std::endl;
  std::cout << "--------------------" << std::endl;

  std::string indentation2 = "";

  printTree(myTree, indentation2);

  std::cout << "--------------------" << std::endl << std::endl;

}
